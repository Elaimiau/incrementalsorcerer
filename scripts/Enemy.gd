extends KinematicBody2D

export var enemy_type : String

var direction : Vector2 = Vector2()
var speed : int = 100

var health : int = 5000

func _ready():
	$Body/AnimationPlayer.play("Move")
	$Body/Hitbox.connect("area_entered", self, "get_hit")

func _physics_process(_delta):
	move_and_slide(direction * speed)

func receive_damage(damage : BulletDamage):
	health -= damage.damage
	if health < 0:
		die()

func get_hit(area):
	$Body/Hit.play("Hit")
	area.get_parent().hitted()
	receive_damage(area.get_parent().damage)

func die():
	queue_free()
