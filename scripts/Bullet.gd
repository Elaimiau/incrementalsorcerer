extends Node2D

var speed = 300
var direction : Vector2 = Vector2(1,1)
var damage : BulletDamage

func _process(delta):
	self.global_position += direction.normalized() * speed * delta

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func hitted():
	queue_free()
