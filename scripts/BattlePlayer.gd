extends KinematicBody2D

var direction : Vector2 = Vector2()
var shooting = false

onready var base_bullet = preload("res://scenes/Bullet.tscn")
onready var global_stats = $"/root/GlobalsStats"
onready var shoot_timer = $Shoot
onready var animations = $AnimationPlayer

var animation = "idle"

var current_bullet_damage : BulletDamage

func hit(_area):
	print("player got hit!")

func _physics_process(_delta):
	var velocity : = direction.normalized() * 400
	move_and_slide(velocity)
	

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		shooting = event.is_pressed()
		if shooting:
			shoot_timer.start()
			_on_Shoot_timeout()
		else:
			shoot_timer.stop()

func _unhandled_key_input(event):
	if event.is_action_pressed("down"):
		direction.y = 1
		animation = "down"
	if event.is_action_pressed("left"):
		direction.x = -1
		animation = "left"
	if event.is_action_pressed("right"):
		direction.x = 1
		animation = "right"
	if event.is_action_pressed("up"):
		direction.y = -1
		animation = "up"
	
	if event.is_action_released("down") and direction.y == 1:
		direction.y = 0
	if event.is_action_released("left") and direction.x == -1:
		direction.x = 0
	if event.is_action_released("right") and direction.x == 1:
		direction.x = 0
	if event.is_action_released("up") and direction.y == -1:
		direction.y = 0
	
	## Animations
	var moving = direction != Vector2()
	if animation == "left":
		if moving:
			animations.play("Left")
		else:
			animations.play("IdleLeft")
	if animation == "right":
		if moving:
			animations.play("Right")
		else:
			animations.play("IdleRight")
	if animation == "up":
		if moving:
			animations.play("Up")
		else:
			animations.play("IdleUp")
	if animation == "down":
		if moving:
			animations.play("Down")
		else:
			animations.play("IdleDown")


func _on_Shoot_timeout():
	if not current_bullet_damage:
		current_bullet_damage = $"/root/GlobalsStats".get_current_bullet_damage()
	
	var bullet = base_bullet.instance()
	bullet.damage = current_bullet_damage
	self.get_parent().add_child(bullet)
	bullet.direction = get_local_mouse_position().normalized()
	bullet.global_position = self.global_position
