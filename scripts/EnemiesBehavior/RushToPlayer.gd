extends Node

onready var enemy : KinematicBody2D = get_parent()
onready var current_battle = $"/root/CurrentBattle"
var player

func _process(_delta):
	if not player:
		player = current_battle.get_player()
	var direction_to_player = enemy.global_position.direction_to(player.global_position)
	enemy.direction = direction_to_player.normalized()
