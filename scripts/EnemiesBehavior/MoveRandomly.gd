extends Node

onready var enemy : KinematicBody2D = get_parent()
onready var current_battle = $"/root/CurrentBattle"
var player

func _ready():
	var timer = enemy.get_node("MovementTimer")
	timer.connect("timeout", self, "change_direction")

func _process(_delta):
	pass

func change_direction():
	var x = (randi() % 100) - 50
	var y = (randi() % 100) - 50
	enemy.direction = Vector2(x, y).normalized()
