extends Node

var player_bullet_power = 100
var player_bullet_size = 100
var player_bullet_fire_rate = 100

func _ready():
	# init global random seed
	randomize()

func get_current_bullet_damage() -> BulletDamage:
	var damage : = BulletDamage.new()
	damage.damage = player_bullet_power
	return damage
