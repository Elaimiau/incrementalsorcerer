extends Node

var movement_behaviors : Dictionary = {
	"rush_to_player": null,
	"move_randomly": null
}

var shooting_scenes : Dictionary = {
	"no_shooting": "NotShooting.gd",
}

func _ready():
	var rush_script = File.new()
	rush_script.open("res://scripts/EnemiesBehavior/RushToPlayer.gd", File.READ)
	movement_behaviors["rush_to_player"] = rush_script
